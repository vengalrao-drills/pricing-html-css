document.getElementById("check").addEventListener("change", function () {
  let monthly = document.querySelector(".monthly");
  let annually = document.querySelector(".annually");
  console.log(monthly.innerHTML);
  console.log(this.checked);
  if (this.checked) {
    monthly.classList.remove("none");
    annually.classList.add("none");
  } else {
    monthly.classList.add("none");
    annually.classList.remove("none");
  }
});
